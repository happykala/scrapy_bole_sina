import base64
from urllib import parse
import time
import re
import json
import requests


def use_encrypt(user):
    """
    用户名加密函数
    :param user:
    :return:
    """
    user = parse.quote(user)
    user = base64.b64encode(user.encode())
    return user


# def vister(self):
#      headers1 = {
#         "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
#     }
#     data1 = {
#         "entry": "miniblog",
#         "a": "enter",
#         "url": "https://weibo.com/",
#         "domain": ".weibo.com",
#         "ua": "php-sso_sdk_client-0.6.23",
#         "_rand": round(time.time(), 4),
#     }
#     ss = requests.Session()
#     r = ss.get(start_url, headers=headers1, params=data1)
#     url = r.url


def prelogin():
        # 预登陆，拿到服务器返回的加密信息；
        prelogin_url = 'https://login.sina.com.cn/sso/prelogin.php'
        params = {
            "entry": "weibo",
            "callback": "sinaSSOController.preloginCallBack",
            'su': '',
            "rsakt": "mod",
            "client": "ssologin.js(v1.4.19)",
            "_": int(time.time() * 1000)
        }
        content = requests.session().get(prelogin_url, params=params)

        try:
            pattern = re.compile(r'sinaSSOController.preloginCallBack\((.+?)\)')
            content = re.findall(pattern, content.text)[0]
            js = json.loads(content)
            # self.servertime = js['servertime']
            # self.nonce = js['nonce']
            # self.pubkey = js['pubkey']
            # self.rsakv = js['rsakv']
            # self.exectime = js['exectime']
            print(js)
        except:
            print('json解析不了')

prelogin()